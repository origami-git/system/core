option('platform', type : 'combo',
	choices : [ 'bios', 'uefi' ],
	value : 'bios'
)

# drivers
option('drv_example', type : 'boolean', value : true, description : 'Example driver', yield : true)
option('drv_fs-ramfs', type : 'boolean', value : true, description : 'Temporary (memory-backed) file system driver support', yield : true)
option('drv_fs-sysfs', type : 'boolean', value : true, description : 'Sysfs (ramfs-based) file system driver support', yield : true)
