#!/usr/bin/env nix-shell
{ pkgs ? import <nixpkgs> {
  crossSystem = {
    config = "i686-elf";
  };
} }:
pkgs.mkShell {
  shellHook = ''
    export PATH="$(realpath ../../scripts/bin):$(realpath scripts/bin):$PATH"
  '';

  nativeBuildInputs = with pkgs.buildPackages; [
    meson
    ninja

    gdb
    nasm
    xorriso
    grub2
    buildPackages.qemu
  ];
}

