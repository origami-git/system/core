/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <fs/vfs.hpp>

struct context {

	void* _address;
};

struct f_context : context {

	red_moon::fs::mount_point _mount;

	const char* _filename;
	const char* _filepath;

	uint64 _data_size;
	uint64 _data_pos;

	void* r;
};
