/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <util/std/mem.hpp>
#include <util/std/string.hpp>

namespace red_moon::util {

	template <typename K, typename V>
	class Dictionary {
	public:
		struct Item {
			K key;
			V value;
			Item* next;

			Item(K k, V v) : key(k), value(v), next(nullptr) { }
		};
	private:
		Item* item;
	public:
		Item* first;
		size_t count;

		Dictionary() : item(nullptr), first(nullptr), count(0) { }

		~Dictionary() {
			Item* current = first;

			while(current != nullptr) {
				Item* temp = current;
				current = current->next;
				
				delete temp;
			}

			count = 0;
		}

		bool has(K key) {
			if(!first) return false;
			Item* current = first;

			do {
				if(strcmp(current->key, key) == 0) {
					return true;
				}
			} while((current = current->next) != nullptr);

			return false;
		}

		Item* insert(K key, V value) {
			if(first != nullptr && has(key)) return nullptr;

			Item* item = new Item(key, value);
			if(this->item != nullptr) this->item->next = item;
			this->item = item;

			if(first == nullptr) first = item;
			count++;
			return item;
		}

		bool remove(K key) {
			if(!first) return false;

			Item* prev = nullptr;
			Item* current = first;

			do {
				if(strcmp(current->key, key) == 0) {
					prev->next = nullptr;
					delete current;

					count--;
					return true;
				}

				prev = current;
			} while((current = current->next) != nullptr);

			return false;
		}

		bool get(K key, V __ref value) {
			if(!first) return false;
			Item* current = first;

			do {
				if(strcmp(current->key, key) == 0) {
					value = current->value;
					return true;
				}
			} while((current = current->next) != nullptr);

			return false;
		}

		bool get(size_t index, V __ref value) {
			if(!first) return false;
			
			Item* current = first;
			for(size_t i = 0; i < index - 1; i++) {
				current = current->next;
				if(current == nullptr) return false;
			}

			value = current->value;
			return true;
		}

		Item* get(K key) {
			if(!first) return nullptr;
			Item* current = first;

			do {
				if(strcmp(current->key, key) == 0) {
					return current;
				}
			} while((current = current->next) != nullptr);

			return nullptr;
		}

		bool set(K key, V value) {
			if(!first) return false;
			Item* current = first;

			do {
				if(strcmp(current->key, key) == 0) {
					current->value = value;
					return true;
				}
			} while((current = current->next) != nullptr);

			return false;
		}
	};
}
