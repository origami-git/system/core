/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>

namespace red_moon::util {

	template <typename F>
	class FieldMap {
	public:
		F field;
	public:
		FieldMap(F field) : field(field) { }

		operator F() const {
			return field;
		}

		template<int bit>
		bool bool_field(bool set, bool value = false) {
			if(set) {
				if(value) {
					field |= (1U << bit);
				} else {
					field &= ~(1U << bit);
				}
			}

			return (field & (1U << bit)) != 0;
		}
	};
}
