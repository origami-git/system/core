/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <stddef.h>
#include <util/types.hpp>

namespace red_moon::util {

	struct bitmap {

		byte* buffer;
		size_t buffer_size;

		bool get(uint64 index) {
			assert(index < buffer_size, "index > buffer_size")
			if(index > buffer_size) return false;

			uint64 byteIndex = index / 8;
			byte bitIndex = index % 8;
			byte bitIndexer = 0b10000000 >> bitIndex;

			return (buffer[byteIndex] & bitIndexer) > 0;
		}

		bool set(uint64 index, bool value) {
			assert(index < buffer_size, "index > buffer_size")

			uint64 byteIndex = index / 8;
			byte bitIndex = index % 8;
			byte bitIndexer = 0b10000000 >> bitIndex;

			bool old_val = (buffer[byteIndex] & bitIndexer) > 0;

			buffer[byteIndex] &= ~bitIndexer;

			if(value) {
				buffer[byteIndex] |= bitIndexer;
			}

			return old_val;
		}
	};
}
