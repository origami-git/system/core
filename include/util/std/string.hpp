/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <stddef.h>

extern "C" {
	size_t strlen(const char* str);
	char* strcat(char* dest, const char* src);
	char* strcpy(char* dest, const char* src);
	int strcmp(const char* str1, const char* str2);
	int strncmp(const char* str1, const char* str2, size_t n);
	char* strncpy(char* dest, const char* src, size_t n);
	char* reverse(char* str, size_t length);
	char* itoa(int value, char* str, int base);
}
