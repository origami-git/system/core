/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <stddef.h>
#include <mem/heap.hpp>

extern "C" {
	void* memset(void* start, int value, size_t num);
	void* memcpy(void* destination, const void* source, size_t num);

	inline void* malloc(size_t size) {
		return red_moon::mem::alloc(size);
	}

	inline void free(void* address) {
		red_moon::mem::free(address);
	}

	void* realloc(void* ptr, size_t old_size, size_t new_size);
}
