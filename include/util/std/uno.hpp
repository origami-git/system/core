/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>

extern "C" {

	void strsplit(const char* input, char delimiter, char* parts[],
				  size_t parts_length, size_t& count, bool skip_empty = false);
}
