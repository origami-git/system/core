/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <stdint.h>
#include <stddef.h>
#include <panic.hpp>
#include <util/ext/new.hpp>
//#include <util/ext/local_static_variables.hpp>

#define assert(condition, error_message) if(!(condition)) { panic(error_message); }

#define __out *
#define __in  *
#define __ref &

template<class T, size_t N>
constexpr size_t countof(T (&)[N]) { return N; }

typedef unsigned char byte;

typedef   int8_t   int8;
typedef  int16_t  int16;
typedef  int32_t  int32;
typedef  int64_t  int64;
typedef  uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
