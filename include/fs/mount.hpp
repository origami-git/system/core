/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <hw/device.hpp>
#include <mod/fs.hpp>

namespace red_moon::fs {
	
	bool mount(const char* device, const char* path, const char* driver);
	bool unmount(const char* path);
}
