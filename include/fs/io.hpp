/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <hw/context.hpp>

namespace red_moon::fs {
	
	bool fcreate(const char* path, f_context __ref ctx);
	bool fdelete(const char* path);

	bool mkdir(const char* path);
	bool rmdir(const char* path);

	f_context fopen(const char* path);
	bool fclose(f_context __ref ctx);

	bool rename(f_context ctx, const char* name);

	size_t fread(f_context __ref ctx, size_t begin, size_t end, byte __out data);
	size_t fwrite(f_context __ref ctx, byte data[], size_t begin, size_t length);
}
