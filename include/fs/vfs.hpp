/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/dict.hpp>
#include <hw/device.hpp>
#include <mod/driver.hpp>

namespace red_moon::fs {

	struct mount_point {
		hw::Device* device;
		mod::Driver* driver;

		const char* path;
	};

	extern util::Dictionary<const char*, mount_point*>* mounts;

	bool vfs_init();
}
