/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#ifdef MOD_DRV_FS_SYSFS
#include <hw/context.hpp>
#include <mod/drivers/fs/sysfs.hpp>

namespace red_moon::fs {

	bool sysio_register(const char* path, bool (*handler)(bool, byte*, size_t __ref));
	bool sysio_remove(const char* path);
}
#endif
