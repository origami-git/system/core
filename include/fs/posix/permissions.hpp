/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>

namespace red_moon::fs::posix {

	struct permission_group {

		bool read;
		bool write;
		bool execute;

		permission_group(bool r, bool w, bool x)
			: read(r), write(w), execute(x) { }
	};

	struct file_permissions {

		permission_group owner;
		permission_group group;
		permission_group user;

		file_permissions(permission_group o,
						 permission_group g,
						 permission_group u)
			: owner(o), group(g), user(u) { }

		file_permissions() : owner(permission_group(true, true, false)),
							 group(permission_group(true, false, false)),
							 user(permission_group(true, false, false)) { }
	};
}
