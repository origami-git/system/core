/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <fs/posix/attributes.hpp>
#include <hw/context.hpp>

struct inode {

	uint64 id;

	const char* name;
	const char* path;

	uint16 ouid;
	uint16 ogid;

	red_moon::fs::posix::file_attributes mode;

	uint64 ctime;
	uint64 mtime;
	uint64 atime;

	uint16 link_count;

	byte* data;
	uint64 size;

	inode() : id(0), name(""), path(""),
			  ouid(0), ogid(0), mode(),
			  ctime(0), mtime(0), atime(0),
			  link_count(0), data(nullptr), size(0) { }

	inode(uint64 id, const char* name, const char* path,
		  red_moon::fs::posix::file_type type, byte* data, uint64 size)
		: id(id), name(name), path(path),
		  ouid(0), ogid(0),
		  ctime(0), mtime(0), atime(0),
		  link_count(0), data(data), size(size) {
		
		this->mode = {
			.setuid = false,
			.setgid = false,
			.type = type,
			.permissions = red_moon::fs::posix::file_permissions()
		};
	}
};
