/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <fs/posix/permissions.hpp>

namespace red_moon::fs::posix {

	enum file_type : uint16 {

		File,
		Directory,
		Symlink,
		Chardev,
		Pseudoterm,
		Blockdev,
		Socket
	};

	struct file_attributes {

		bool setuid;
		bool setgid;

		file_type type;
		file_permissions permissions;
	};
}
