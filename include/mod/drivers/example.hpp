/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <mod/driver.hpp>

namespace red_moon::mod::drivers {

	class ExampleDriver : public mod::Driver {
	public:
		ExampleDriver() : Driver("none", "example") { }
		~ExampleDriver() override { }

		bool _entrypoint();
		bool _exit();
	};
}
