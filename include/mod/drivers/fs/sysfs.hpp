/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <mod/drivers/fs/ramfs.hpp>

using namespace red_moon::fs::posix;

namespace red_moon::mod::drivers {

	class SysfsDriver : public RamfsDriver {
		typedef bool (*SysfsIoHandler)(bool, byte*, size_t __ref);
	public:
		SysfsDriver() : RamfsDriver("sysfs") {
			static dirnode only_mount(nullptr);
			this->mounts->insert("sysfs", only_mount);
		}

		/* TODO learn why there needs to be a noexcept here */
		~SysfsDriver() noexcept override { }

		bool _init_mount(fs::mount_point mp, const char* recursive_path = "");
		bool _deinit_mount(fs::mount_point mp, const char* recursive_path = "");

		bool _create(fs::mount_point mp, const char* path, uint16 type, f_context __ref ctx);
		bool _delete(fs::mount_point mp, const char* path);

		size_t _list(fs::mount_point mp, const char* path, inode* __out files);

		bool _open(fs::mount_point mp, const char* path, f_context __ref ctx);
		bool _close(fs::mount_point mp, f_context __ref ctx);

		bool _rename(fs::mount_point mp, f_context ctx, const char* name);

		size_t _read(fs::mount_point mp, f_context __ref ctx, size_t begin, size_t end, byte __out data);
		size_t _write(fs::mount_point mp, f_context __ref ctx, byte __in data, size_t begin, size_t length);

		bool sysio_export(fs::mount_point mp, const char* path, SysfsIoHandler handler);
		bool sysio_unreg(fs::mount_point mp, const char* path);
	};
}
