/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <fs/posix/inode.hpp>
#include <mod/drivers/fs/ramfs/file.hpp>

namespace red_moon::mod::drivers::ramfs {

	struct dirnode : public filenode {

		util::Dictionary<const char*, filenode>* files;

		dirnode() { }

		dirnode(inode* node) : filenode(node),
			files(new util::Dictionary<const char*, filenode>) { }
	};
}
