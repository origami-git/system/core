/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <fs/posix/inode.hpp>

namespace red_moon::mod::drivers::ramfs {

	struct filenode {

		inode* node;

		filenode() : node(nullptr) { }
		filenode(inode* node) : node(node) { }
	};
}
