/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <mod/fs.hpp>
#include <util/dict.hpp>
#include <mod/drivers/fs/ramfs/file.hpp>
#include <mod/drivers/fs/ramfs/dir.hpp>
#include <fs/vfs.hpp>

/*
TODO
- specifying max size (and monitoring memory usage)
*/
using namespace red_moon::mod::drivers::ramfs;
using namespace red_moon::fs::posix;

namespace red_moon::mod::drivers {

	class RamfsDriver : public FilesystemDriver {
	protected:
		#define MAX_DEPTH 100

		util::Dictionary<const char*, dirnode>* mounts;
	private:
		uint64 last_id = 0;
	public:
		/* for fs drivers that base off of ramfs */
		RamfsDriver(const char* identifier) : FilesystemDriver(identifier) {
			mounts = new util::Dictionary<const char*, dirnode>;
		}

		RamfsDriver() : FilesystemDriver("ramfs") {
			mounts = new util::Dictionary<const char*, dirnode>;
		}

		/* TODO learn why there needs to be a noexcept here */
		~RamfsDriver() noexcept override { }

		bool _entrypoint() { return true; }
		bool _exit() { delete mounts; return true; }

		bool _init_mount(fs::mount_point mp, const char* recursive_path = "");
		bool _deinit_mount(fs::mount_point mp, const char* recursive_path = "");

		bool _create(fs::mount_point mp, const char* path, uint16 type, f_context __ref ctx);
		bool _delete(fs::mount_point mp, const char* path);

		size_t _list(fs::mount_point mp, const char* path, inode* __out files);

		bool _open(fs::mount_point mp, const char* path, f_context __ref ctx);
		bool _close(fs::mount_point mp, f_context __ref ctx);

		bool _rename(fs::mount_point mp, f_context ctx, const char* name);

		size_t _read(fs::mount_point mp, f_context __ref ctx, size_t begin, size_t end, byte __out data);
		size_t _write(fs::mount_point mp, f_context __ref ctx, byte __in data, size_t begin, size_t length);
	};
}
