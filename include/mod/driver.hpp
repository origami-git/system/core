/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>

namespace red_moon::mod {

	#define MAX_LOADED 100

	class Driver {
	public:
		const char* prefix;
		const char* identifier;

		/* gets called when kernel is loading the driver into memory */
		Driver(const char* prefix, const char* identifier) {
			this->prefix = prefix;
			this->identifier = identifier;
		}

		/* gets called when the kernel is completely unloading the driver */
		virtual ~Driver() = default;

		virtual bool _entrypoint() = 0;	/* gets called when the driver is being enabled  (e.g. modprobe)    */
		virtual bool _exit() = 0;		/* gets called when the driver is being disabled (e.g. modprobe -r) */
	};

	namespace driver {
		extern Driver* drivers[MAX_LOADED];

		bool load(Driver* driver);
		bool unload(Driver* driver);

		Driver* get(const char* prefix, const char* identifier);
	}
}
