/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <mod/driver.hpp>
#include <fs/vfs.hpp>
#include <hw/context.hpp>
#include <fs/posix/inode.hpp>

namespace red_moon::mod {

	/* oh */
	class FilesystemDriver : public Driver {
	public:
		FilesystemDriver(const char* identifier) : Driver("fs", identifier) { }

		virtual ~FilesystemDriver() = default;

		virtual bool _init_mount(fs::mount_point mp, const char* recursive_path = "") = 0;
		virtual bool _deinit_mount(fs::mount_point mp, const char* recursive_path = "") = 0;

		virtual bool _create(fs::mount_point mp, const char* path, uint16 type, f_context __ref ctx) = 0;
		virtual bool _delete(fs::mount_point mp, const char* path) = 0;

		virtual size_t _list(fs::mount_point mp, const char* path, inode* __out files) = 0;

		virtual bool _open(fs::mount_point mp, const char* path, f_context __ref ctx) = 0;
		virtual bool _close(fs::mount_point mp, f_context __ref ctx) = 0;

		virtual bool _rename(fs::mount_point mp, f_context ctx, const char* name) = 0;

		virtual size_t _read(fs::mount_point mp, f_context __ref ctx, size_t begin, size_t end, byte __out data) = 0;  /* returns amount of bytes read    */
		virtual size_t _write(fs::mount_point mp, f_context __ref ctx, byte __in data, size_t begin, size_t length) = 0; /* returns amount of bytes written */
	};
}
