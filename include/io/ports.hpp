/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <stdint.h>

static inline void outb(unsigned short port, unsigned char val) { asm volatile ( "out %0, %1" : : "a"(val), "Nd"(port) :"memory"); }

static inline uint8_t inb(unsigned short port) {
	uint8_t ret;

	asm volatile("inb %1, %0"
				: "=a"(ret)
				: "Nd"(port)
				: "memory");
				
	return ret;
}
