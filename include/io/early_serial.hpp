/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <io/ports.hpp>

#define COM1 0x3F8
#define COM2 0x2F8
#define COM3 0x3E8
#define COM4 0x2E8
#define COM5 0x5F8
#define COM6 0x4F8
#define COM7 0x5E8
#define COM8 0x4E8

#define BAUD_RATE 0x03 /* baud rate divisor value, 0x03 = 38400 baud */

extern unsigned short port;

static inline bool _eserial_available_read() { return inb(port + 5) & 1; }
static inline bool _eserial_able_send() { return inb(port + 5) & 0x20; }

bool eserial_init(unsigned short port);

char eserial_read();
void eserial_readline(char** buffer);

void eserial_write(char c);
void eserial_write(const char* s);
