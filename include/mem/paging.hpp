/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <mem/paging/directory_manager.hpp>
#include <mem/paging/table_manager.hpp>
#include <mem/paging/frame_manager.hpp>
#include <util/types.hpp>

namespace red_moon::mem {
	
	#define PAGE_SIZE 0x1000

	namespace paging {

		extern PageFrameManager frame_manager;
		extern PageDirectoryManager directory_manager;
	}

    bool paging_init(void* memory_start, size_t memory_size);
	bool paging_enable();

	bool map_memory(void* paddress, void* vaddress, bool zero = false,
					paging::PageTableManager** table = nullptr, paging::PageTableEntry** entry = nullptr);
	
	bool unmap_memory(void* vaddress);
}
