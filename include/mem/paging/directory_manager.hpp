/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <mem/paging/table_manager.hpp>
#include <mem/paging/frame_manager.hpp>
#include <util/types.hpp>
#include <util/fieldmap.hpp>

namespace red_moon::mem::paging {

	class PageDirectoryEntry : public red_moon::util::FieldMap<uint32> {
	private:
		template<typename T>
		T address_field(bool set, T value = 0) {
			if(set) {
				value >>= 12;
				value = value & 0xFFFFF;
				field = field & (~(0xFFFFF << 12));
				field = field | (value << 12);
			}
			
			return (field >> 12) << 12; /* returns page-aligned address */
		}
	public:
		PageDirectoryEntry() : FieldMap(0b00000000000000000000000000000010) { }
		PageDirectoryEntry(uint32 value) : FieldMap(value) { }

		bool present(bool set = false, bool value = false) { return bool_field<0>(set, value); }
		bool rw(bool set = false, bool value = true) { return bool_field<1>(set, value); }
		bool supervisor(bool set = false, bool value = false) { return bool_field<2>(set, value); }
		bool writethrough(bool set = false, bool value = false) { return bool_field<3>(set, value); }
		bool nocache(bool set = false, bool value = false) { return bool_field<4>(set, value); }
		bool accessed(bool set = false, bool value = false) { return bool_field<5>(set, value); }
		bool available0(bool set = false, bool value = false) { return bool_field<6>(set, value); }
		bool pagesize(bool set = false, bool value = false) { return bool_field<7>(set, value); }
		//uint8 available1(bool set = false, uint8 value = 0) { return int_field<uint8, 8, 4>(set, value); }
		uint32 address(bool set = false, uint32 value = 0) { return address_field<uint32>(set, value); }
	};

	class PageDirectoryManager {
		#define PAGE_SIZE			0x1000
		#define PAGE_DIRECTORY_SIZE 1024
	protected:
		PageFrameManager* frame_manager;

		/* this NEEDS to be static, otherwise the stack gets trashed in paging.cpp */
		/* to be investigated further */
		static PageDirectoryEntry entries[PAGE_DIRECTORY_SIZE] __attribute__((aligned(PAGE_SIZE))); 
		static PageTableManager* tables[PAGE_TABLE_SIZE] __attribute__((aligned(PAGE_SIZE)));
	public:
		PageDirectoryManager() {
			// only here because C++
		}
		
		PageDirectoryManager(PageFrameManager* frame_manager) {
			this->frame_manager = frame_manager;

			for(int i = 0; i < PAGE_DIRECTORY_SIZE; i++) {
				this->entries[i] = PageDirectoryEntry();
			}
		}

		void* get();

		PageDirectoryEntry* entry(uint16 index);
		PageDirectoryEntry* entry(void* address);

		PageTableManager* table(uint16 index);
		PageTableManager* table(void* address);
	};
}
