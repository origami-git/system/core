/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <util/fieldmap.hpp>

namespace red_moon::mem::paging {

	class PageTableEntry : public red_moon::util::FieldMap<uint32> {
	private:
		template<typename T>
		T address_field(bool set, T value = 0) {
			if(set) {
				value >>= 12;
				value = value & 0xFFFFF;
				field = field & (~(0xFFFFF << 12));
				field = field | (value << 12);
			}
			
			return (field >> 12) << 12; /* returns page-aligned address */
		}
	public:
		PageTableEntry() : FieldMap(0b00000000000000000000000000000010) { }
		PageTableEntry(uint32 value) : FieldMap(value) { }

		bool present(bool set, bool value = false) { return bool_field<0>(set, value); }
		bool rw(bool set, bool value = true) { return bool_field<1>(set, value); }
		bool supervisor(bool set, bool value = false) { return bool_field<2>(set, value); }
		bool writethrough(bool set, bool value = false) { return bool_field<3>(set, value); }
		bool nocache(bool set, bool value = false) { return bool_field<4>(set, value); }
		bool accessed(bool set, bool value = false) { return bool_field<5>(set, value); }
		bool dirty(bool set, bool value = false) { return bool_field<6>(set, value); }
		bool pat(bool set, bool value = false) { return bool_field<7>(set, value); }
		bool global(bool set, bool value = false) { return bool_field<8>(set, value); }
		//uint8 available0(bool set, uint8 value = 0) { return int_field<uint8, 9, 3>(set, value); }
		uint32 address(bool set, uint32 value = 0) { return address_field<uint32>(set, value); }
	};

	class PageTableManager {
		#define PAGE_SIZE		0x1000
		#define PAGE_TABLE_SIZE 1024
	private:
		PageTableEntry entries[PAGE_TABLE_SIZE] __attribute__((aligned(PAGE_SIZE)));
	public:
		PageTableManager() { }

		void* get();

		PageTableEntry* entry(uint16 index);
		PageTableEntry* entry(void* address);
	};
}
