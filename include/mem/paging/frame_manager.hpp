/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <util/bitmap.hpp>
#include <util/std/math.hpp>

extern size_t _kernel_begin_addr;
extern size_t _kernel_end_addr;

namespace red_moon::mem::paging {

	struct page_frame {
		uint64 index;
		void* paddress;
	};

	class PageFrameManager {
		#define PAGE_SIZE 0x1000
	private:
		size_t memory_base;
		uint64 recent_index;
	public:
		static red_moon::util::bitmap frame_map;

		size_t m_used = 0;
		size_t m_free = 0;

		PageFrameManager() {
			// this is only here because C++
		}

		PageFrameManager(size_t memory_start, size_t memory_size) {
			this->memory_base = memory_start;
			this->recent_index = 0;

			/* initialize bitmap at [_kernel_end_addr + 0x1000] */
			/* actually, here we assume that kernel starts at memory_start (it does for now)
			   and that it ends at +0x20000, which unfortunately I am not able to verify
			   as for WHATEVER REASON _kernel_end_addr < _kernel_begin_addr */
			/* we pretty much just hope that the kernel + everything in it doesn't exceed 128 KiB */
			this->frame_map.buffer = (uint8*) (memory_start + 0x20000 + PAGE_SIZE);
			this->frame_map.buffer_size = memory_size / PAGE_SIZE / 8;

			for(size_t i = 0; i < this->frame_map.buffer_size; i++) {
				*(uint8*) (this->frame_map.buffer + i) = 0;
			}

			/* lock kernel pages */
			lock((void*) memory_base, ((0x20000) / PAGE_SIZE) + 1);

			/* ...and also bitmap pages */
			lock(this->frame_map.buffer, (this->frame_map.buffer_size / PAGE_SIZE) + 1);
		}

		page_frame* alloc();
		size_t alloc(size_t count, page_frame** __out frames = nullptr);

		page_frame* lock(void* paddress);
		size_t lock(void* paddresses[], size_t count, page_frame** __out frames = nullptr);
		size_t lock(void* paddress, size_t count, page_frame** __out frames = nullptr);

		bool free(void* paddress);
		size_t free(void* paddresses[], size_t count);
		size_t free(void* paddress, size_t count);
	};
}
