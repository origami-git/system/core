/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <util/types.hpp>
#include <mem/heap/allocator.hpp>
#include <mem/heap/manager.hpp>

namespace red_moon::mem {

	namespace heap {

		extern Heap manager;
		extern Allocator allocator;
	}

	bool heap_init(void* address, size_t size);
	
	inline void* alloc(size_t size) {
		return heap::allocator.alloc(size);
	}

	inline void free(void* address) {
		heap::allocator.free(address);
	}
}
