/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <mem/heap/manager.hpp>

namespace red_moon::mem::heap {

	class Allocator {
	private:
		Heap* heap;
	public:
		Allocator() {
			// only here to satisfy C++
		}
		
		Allocator(Heap* heap) {
			this->heap = heap;
		}

		void* alloc(size_t size);
		void free(void* address);
	};
}
