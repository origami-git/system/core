/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

namespace red_moon::mem::heap {
	class Heap;
}

#pragma once
#include <util/types.hpp>

namespace red_moon::mem::heap {

	struct fragment {
		bool free;
		size_t size;

		void* heap;

		fragment* next;
		fragment* prev;

		fragment* split(size_t size);
		void join(bool forward);
	};
}
