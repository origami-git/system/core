/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

namespace red_moon::mem::heap {
	struct fragment;
}

#pragma once
#include <util/types.hpp>
#include <mem/paging.hpp>
#include <mem/heap/fragment.hpp>

namespace red_moon::mem::heap {

	class Heap {
	#define BASE_ADDRESS 0xA0000000
	public:
		void* start_address;
		void* end_address;

		fragment* recent_header;

		Heap() {
			// only here because C++
		}

		Heap(void* address, size_t size) {
			this->start_address = (void*) BASE_ADDRESS;
			this->end_address = (void*) (BASE_ADDRESS + size);

			paging::frame_manager.lock(address, ceil(size / PAGE_SIZE));
			
			for(size_t i = 0; i < size; i += PAGE_SIZE) {
				map_memory((void*) ((size_t) address + i), (void*) (BASE_ADDRESS + i), true);
			}

			fragment* start_fragment = (fragment*) this->start_address;
			start_fragment->heap = this;
			start_fragment->size = size - sizeof(fragment);
			start_fragment->next = nullptr;
			start_fragment->prev = nullptr;
			start_fragment->free = true;

			recent_header = start_fragment;
		}

		void expand(size_t size);
	};
}
