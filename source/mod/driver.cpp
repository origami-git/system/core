/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mod/driver.hpp>
#include <util/std/string.hpp>
#include <util/types.hpp>
#include <panic.hpp>

namespace red_moon::mod::driver {

	Driver* drivers[MAX_LOADED] = { nullptr };

	bool load(Driver* driver) {
		for(int i = 0; i < MAX_LOADED; i++) {
			if(drivers[i] == nullptr) {
				drivers[i] = driver;

				return true;
			}
		}

		panic("len(driver::loaded) > MAX_LOADED");
		return false;
	}

	bool unload(Driver* driver) {
		for(size_t i = 0; i < MAX_LOADED; i++) {
			if(drivers[i] == driver) {
				drivers[i] = nullptr;
				delete driver;

				return true;
			}
		}

		return false;
	}

	Driver* get(const char* prefix, const char* identifier) {
		for(Driver* driver : drivers) {
			if(strcmp(driver->prefix, prefix) == 0
				&& strcmp(driver->identifier, identifier) == 0) {
				
				return driver;
			}
		}

		return nullptr;
	}
}
