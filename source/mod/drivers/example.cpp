/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mod/drivers/example.hpp>

namespace red_moon::mod::drivers {

	bool ExampleDriver::_entrypoint() {
		return true;
	}

	bool ExampleDriver::_exit() {
		return false;
	}
}
