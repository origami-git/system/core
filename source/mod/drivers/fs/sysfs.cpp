/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mod/drivers/fs/sysfs.hpp>
#include <util/std/uno.hpp>

namespace red_moon::mod::drivers {

	bool SysfsDriver::_init_mount(fs::mount_point mp, const char* recursive_path /*= ""*/) {
		return true;
	}

	bool SysfsDriver::_deinit_mount(fs::mount_point mp, const char* recursive_path /*= ""*/) {
		return false;
	}

	bool SysfsDriver::_create(fs::mount_point mp, const char* path, uint16 type, f_context __ref ctx) {
		/* mp.path = "sysfs";
		return RamfsDriver::_create(mp, path, type, ctx); */
		return false;
	}

	bool SysfsDriver::_delete(fs::mount_point mp, const char* path) {
		/* mp.path = "sysfs";
		return RamfsDriver::_delete(mp, path); */
		return false;
	}

	size_t SysfsDriver::_list(fs::mount_point mp, const char* path, inode* __out files) {
		mp.path = "sysfs";
		return RamfsDriver::_list(mp, path, files);
	}

	bool SysfsDriver::_open(fs::mount_point mp, const char* path, f_context __ref ctx) {
		mp.path = "sysfs";
		return RamfsDriver::_open(mp, path, ctx);
	}

	bool SysfsDriver::_close(fs::mount_point mp, f_context __ref ctx) {
		mp.path = "sysfs";
		return RamfsDriver::_close(mp, ctx);
	}

	bool SysfsDriver::_rename(fs::mount_point mp, f_context ctx, const char* name) {
		/* mp.path = "sysfs";
		return RamfsDriver::_rename(mp, ctx, name); */
		return false;
	}

	size_t SysfsDriver::_read(fs::mount_point mp, f_context __ref ctx, size_t begin, size_t end, byte __out data) {
		/* mp.path = "sysfs";
		return RamfsDriver::_read(mp, ctx, begin, end, data); */
		inode* node = (inode*) ctx._address;
		
		switch(node->link_count) {
			case 1: {
				SysfsIoHandler handler = (SysfsIoHandler) ((void*) (static_cast<size_t>(node->id)));

				size_t read = 0;
				handler(false, data, read);

				return read;
			} default:
				return 0;
		}
	}

	size_t SysfsDriver::_write(fs::mount_point mp, f_context __ref ctx, byte __in data, size_t begin, size_t length) {
		/* mp.path = "sysfs";
		return RamfsDriver::_write(mp, ctx, data, begin, length); */
		inode* node = (inode*) ctx._address;
		
		switch(node->link_count) {
			case 1: {
				SysfsIoHandler handler = (SysfsIoHandler) ((void*) (static_cast<size_t>(node->id)));
				handler(true, data, length);

				return length;
			}
			default:
				return 0;
		}
	}

	bool SysfsDriver::sysio_export(fs::mount_point mp, const char* path, SysfsIoHandler handler) {
		mp.path = "sysfs";

		f_context ctx;
		if(!RamfsDriver::_create(mp, path, file_type::File, ctx)) return false;

		inode* node = (inode*) ctx._address;
		node->id = (uint64) handler;
		node->link_count = 2;
		
		node->mode.permissions.owner.read = true;
		node->mode.permissions.owner.write = true;
		node->mode.permissions.owner.execute = false;

		node->mode.permissions.group.read = false;
		node->mode.permissions.group.write = false;
		node->mode.permissions.group.execute = false;

		node->mode.permissions.user.read = false;
		node->mode.permissions.user.write = false;
		node->mode.permissions.user.execute = false;

		return true;
	}

	bool SysfsDriver::sysio_unreg(fs::mount_point mp, const char* path) {
		mp.path = "sysfs";
		return RamfsDriver::_delete(mp, path);
	}
}
