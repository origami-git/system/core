/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mod/drivers/fs/ramfs.hpp>
#include <util/std/uno.hpp>

namespace red_moon::mod::drivers {

	bool RamfsDriver::_init_mount(fs::mount_point mp, const char* recursive_path /*= ""*/) {
		if(recursive_path[0] != '\0') {
			/* these falses will make so much more sense
			once I finally add exceptions */
			return false;
		}

		static dirnode new_mount(nullptr);
		return (mounts->insert(mp.path, new_mount)) != nullptr;
	}

	bool RamfsDriver::_deinit_mount(fs::mount_point mp, const char* recursive_path /*= ""*/) {
		if(recursive_path[0] != '\0') {
			return false;
		}
		
		return mounts->remove(mp.path);
	}

	bool RamfsDriver::_create(fs::mount_point mp, const char* path, uint16 type, f_context __ref ctx) {
		dirnode dir;
		if(!mounts->get(mp.path, dir)) return false;
		
		char* hierarchy[MAX_DEPTH];
		size_t count = 0;

		strsplit(path, '/', hierarchy, MAX_DEPTH, count, true);

		if(count <= 0) return false;

		for(size_t i = 0; i < count; i++) {
			filenode file;
			bool res = dir.files->get(hierarchy[i], file);

			if(i + 1 == count) {
				if(res) return false;

				inode* izero = new inode(++last_id, hierarchy[i], path,
					static_cast<file_type>(type), nullptr, 0);
				static filenode zero;

				if(type == file_type::Directory) {
					zero = dirnode(izero);
				} else {
					zero = filenode(izero);
				}

				dir.files->insert(hierarchy[i], zero);

				ctx._address = (void*) izero;
				ctx._data_pos = 0;
				ctx._data_size = izero->size;
				ctx._filename = izero->name;
				ctx._filepath = izero->path;
				ctx._mount = mp;

				return true;
			} else {
				if(!res) return false;
				if(file.node->mode.type != file_type::Directory) return false;
				
				dir = *static_cast<dirnode*>(&file);
			}
		}

		return false;
	}

	bool RamfsDriver::_delete(fs::mount_point mp, const char* path) {
		dirnode dir;
		if(!mounts->get(mp.path, dir)) return false;
		
		char* hierarchy[MAX_DEPTH];
		size_t count = 0;

		strsplit(path, '/', hierarchy, MAX_DEPTH, count, true);

		if(count <= 0) return false;

		for(size_t i = 0; i < count; i++) {
			filenode file;
			bool res = dir.files->get(hierarchy[i], file);

			if(i + 1 == count) {
				if(!res) return false;
				if(file.node->mode.type == file_type::Directory) return false;

				dir.files->remove(hierarchy[i]);
				return true;
			} else {
				if(!res) return false;
				if(file.node->mode.type != file_type::Directory) return false;
				
				dir = *static_cast<dirnode*>(&file);
			}
		}

		return false;
	}

	size_t RamfsDriver::_list(fs::mount_point mp, const char* path, inode* __out files) {
		dirnode dir;
		if(!mounts->get(mp.path, dir)) return 0;

		char* hierarchy[MAX_DEPTH];
		size_t count = 0;

		strsplit(path, '/', hierarchy, MAX_DEPTH, count, true);

		if(count <= 0) return 0;

		for(size_t i = 0; i < count; i++) {
			filenode file;
			bool res = dir.files->get(hierarchy[i], file);

			if(i == count) {
				if(!res) return false;
				if(file.node->mode.type != file_type::Directory) return false;

				dir = *static_cast<dirnode*>(&file);
				size_t listed = 0;

				for(size_t j = 0; j < dir.files->count; j++) {
					filenode file;
					if(dir.files->get(j, file)) listed++;

					files[j] = file.node;
				}

				return dir.files->count;
			} else {
				if(!res) return false;
				if(file.node->mode.type != file_type::Directory) return false;
				
				dir = *static_cast<dirnode*>(&file);
			}
		}

		return 0;
	}

	bool RamfsDriver::_open(fs::mount_point mp, const char* path, f_context __ref ctx) {
		dirnode dir;
		if(!mounts->get(mp.path, dir)) return false;
		
		char* hierarchy[MAX_DEPTH];
		size_t count = 0;

		strsplit(path, '/', hierarchy, MAX_DEPTH, count, true);

		if(count <= 0) return false;

		for(size_t i = 0; i < count; i++) {
			filenode file;
			bool res = dir.files->get(hierarchy[i], file);

			if(i + 1 == count) {
				if(!res) return false;

				ctx._address = (void*) file.node;
				ctx._data_pos = 0;
				ctx._data_size = file.node->size;
				ctx._filename = file.node->name;
				ctx._filepath = file.node->path;
				ctx._mount = mp;

				return true;
			} else {
				if(!res) return false;
				if(file.node->mode.type != file_type::Directory) return false;
				
				dir = *static_cast<dirnode*>(&file);
			}
		}

		return false;
	}

	bool RamfsDriver::_close(fs::mount_point mp, f_context __ref ctx) {
		ctx._address = 0;
		ctx._data_pos = 0;
		ctx._data_size = 0;
		ctx._filename = nullptr;
		ctx._filepath = nullptr;
		ctx._mount = fs::mount_point();

		return true;
	}

	/* TODO */
	bool RamfsDriver::_rename(fs::mount_point mp, f_context ctx, const char* name) {
		return false;
	}

	size_t RamfsDriver::_read(fs::mount_point mp, f_context __ref ctx, size_t begin, size_t end, byte __out data) {
		inode* node = (inode*) ctx._address;
		size_t read = 0;
		
		if(node->size < 1 || node->data == nullptr) return 0;

		for(size_t i = begin; i < end; i++) {
			if(begin + i > node->size) return read;

			data[i] = node->data[i];
			read++;
		}

		return read;
	}

	size_t RamfsDriver::_write(fs::mount_point mp, f_context __ref ctx, byte __in data, size_t begin, size_t length) {
		inode* node = (inode*) ctx._address;

		if(begin + length > node->size || node->data == nullptr) {
			byte* new_data = (byte*) malloc(begin + length);
			
			if(node->data != nullptr) {
				memcpy(new_data, node->data, node->size);
				delete node->data;
			}

			node->data = new_data;
			node->size = begin + length;

			ctx._data_size = node->size;
		}

		size_t written = 0;

		for(size_t i = 0; i < length; i++) {
			if(begin + i > node->size) return written;

			node->data[begin + i] = data[i];
			written++;
		}

		return written;
	}
}
