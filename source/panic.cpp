/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <panic.hpp>
#include <io/early_serial.hpp>
#include <util/std/string.hpp>

void panic(const char* message) {
	char m[256];
	strcat(m, "!kernel panic!=");
	strcat(m, message);

	eserial_write(m);
	while(true) { }
}
