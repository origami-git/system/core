/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <util/std/string.hpp>

extern "C" {
	size_t strlen(const char* str) {
		size_t len = 0;

		while(str[len]) {
			len++;
		}

		return len;
	}

	char* strcat(char* dest, const char* src) {
		size_t dest_len = strlen(dest);
		size_t i;

		for(i = 0; src[i] != '\0'; i++) {
			dest[dest_len + i] = src[i];
		}

		dest[dest_len + i] = '\0';
		return dest;
	}

	char* strcpy(char* dest, const char* src) {
		char* d = dest;

		while ((*d++ = *src++));
		return dest;
	}

	int strcmp(const char* str1, const char* str2) {
		while(*str1 && *str2 && *str1 == *str2) {
			str1++;
			str2++;
		}

		if(*str1 == *str2) {
			return 0; // Strings are equal
		} else if(*str1 < *str2) {
			return -1; // str1 is less than str2
		} else {
			return 1; // str1 is greater than str2
		}
	}

	int strncmp(const char* str1, const char* str2, size_t n) {
		for(size_t i = 0; i < n; ++i) {
			if (str1[i] != str2[i]) {
				return (int)(unsigned char) str1[i] - (int)(unsigned char) str2[i];
			}

			// Stop comparing if either string reaches the null-terminator
			if (str1[i] == '\0' || str2[i] == '\0') {
				return 0; // both strings are completely equal
			}
		}

		return 0; // Both strings are equal up to n characters
	}

	char* strncpy(char* dest, const char* src, size_t n) {
		char* d = dest;

		while (n > 0 && *src) {
			*d = *src;
			++d;
			++src;
			--n;
		}

		while (n > 0) {
			*d = '\0';
			++d;
			--n;
		}

		return dest;
	}

	char* reverse(char* str, size_t length) {
		size_t start = 0;
		size_t end = length - 1;
		
		while(start < end) {
			char temp = str[start];

			str[start] = str[end];
			str[end] = temp;

			start++;
			end--;
		}

		return str;
	}

	char* itoa(int value, char* str, int base) {
		if(value == 0) {
			str[0] = '0';
			str[1] = '\0';

			return str;
		}

		int isNegative = 0;
		size_t index = 0;

		if(value < 0 && base == 10) {
			isNegative = 1;
			value = -value;
		}

		while(value != 0) {
			int rem = value % base;

			str[index++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
			value /= base;
		}

		if(isNegative && base == 10) {
			str[index++] = '-';
		}

		str[index] = '\0';

		reverse(str, index);
		return str;
	}
}
