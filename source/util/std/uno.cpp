/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <util/std/uno.hpp>
#include <util/std/string.hpp>

extern "C" {

	void strsplit(const char* input, char delimiter, char* parts[],
					 size_t parts_length, size_t& count, bool skip_empty /*= false*/) {
		
		if(!input) return;

		count = 0;

		const char* start = input;
		for(const char* c = input; *c; ++c) {
			if(*c == delimiter || *(c + 1) == '\0') {
				size_t length = c - start + (*c != delimiter);
				if(length < 1 && skip_empty) goto skip;

				if(count < parts_length) {
					parts[count] = new char[length + 1];
					strncpy(parts[count], start, length);
					parts[count][length] = '\0';
					++count;
				} else {
					return;
				}

			skip:
				if(*c != '\0') {
					start = c + 1;
				}
			}
		}
	}
}
