/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <util/std/mem.hpp>
#include <mem/heap.hpp>

extern "C" {
	void* memset(void* start, int value, size_t num) {
		unsigned char* p = (unsigned char*) start;
		unsigned char x = value & 0xff;

		while(num--) {
			*p++ = x;
		}

		return start;
	}

	void* memcpy(void* destination, const void* source, size_t num) {
		unsigned char* dest = (unsigned char*) destination;
		const unsigned char* src = (const unsigned char*) source;

		for(size_t i = 0; i < num; i++) {
			dest[i] = src[i];
		}

		return destination;
	}

	void* realloc(void* ptr, size_t old_size, size_t new_size) {
		void* newptr = malloc(old_size);
		
		if(newptr == NULL) {
			return NULL;
		}

		memcpy((char*) newptr, (char*) ptr, new_size);
		free(ptr);

		return newptr;
	}
}
