/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <util/std/math.hpp>

extern "C" {
	double floor(double num) {
		return (int) num;
	}

	double ceil(double num) {
		int i_num = (int) num;

		if(i_num < num) {
			i_num += 1;
		}

		return i_num;
	}
}
