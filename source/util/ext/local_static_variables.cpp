/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <util/ext/local_static_variables.hpp>

// TODO actually to do this
namespace __cxxabiv1  {

	extern "C" int __cxa_guard_acquire (__guard *g) {
		return !*(char *)(g);
	}

	extern "C" void __cxa_guard_release (__guard *g) {
		*(char *)g = 1;
	}

	extern "C" void __cxa_guard_abort (__guard *) { }
}
