/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <util/ext/new.hpp>
#include <util/std/mem.hpp>

void *operator new(size_t size)   { return malloc(size); }
void *operator new[](size_t size) { return malloc(size); }
void operator delete(void *p)     { free(p); }
void operator delete[](void *p)   { free(p); }
