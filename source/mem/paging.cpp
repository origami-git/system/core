/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/paging.hpp>
#include <util/std/mem.hpp>

extern "C" void _page_directory_load(unsigned int* paddress);
extern "C" void _paging_enable();

using namespace red_moon::mem::paging;

namespace red_moon::mem {

	namespace paging {

		PageFrameManager frame_manager;
		PageDirectoryManager directory_manager;
	}

	bool paging_init(void* memory_start, size_t memory_size) {
		frame_manager = PageFrameManager((size_t) memory_start, memory_size);
		directory_manager = PageDirectoryManager(&frame_manager);

		return true;
	}

	bool paging_enable() {
		unsigned int* address = (unsigned int*) directory_manager.get();
		_page_directory_load(address);
		_paging_enable();

		return true;
	}

	bool map_memory(void* paddress, void* vaddress, bool zero /*= false*/, PageTableManager** table /*= nullptr*/, PageTableEntry** entry /*= nullptr*/) {
		PageDirectoryEntry* pde = directory_manager.entry(vaddress);
		PageTableManager* ptm = directory_manager.table(vaddress);

		if(table != nullptr) *table = ptm;

		PageTableEntry* pte = ptm->entry(vaddress);
		pte->address(true, (uint32) paddress);
		pte->present(true, true);

		if(entry != nullptr) *entry = pte;

		pde->present(true, true);

		if(zero) memset(vaddress, 0, PAGE_SIZE);
		return true;
	}

	bool unmap_memory(void* vaddress) {
		PageTableManager* ptm = directory_manager.table(vaddress);

		PageTableEntry* pte = ptm->entry(vaddress);
		pte->present(true, false);
		pte->address(true, 0);

		return true;
	}
}
