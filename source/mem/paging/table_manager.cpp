/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/paging/table_manager.hpp>
#include <util/std/math.hpp>

namespace red_moon::mem::paging {

	void* PageTableManager::get() {
		return &entries;
	}

	PageTableEntry* PageTableManager::entry(uint16 index) {
		assert(index < PAGE_TABLE_SIZE, "index > PAGE_TABLE_SIZE");
		return &entries[index];
	}

	PageTableEntry* PageTableManager::entry(void* address) {
		return entry(((size_t) address) / PAGE_SIZE % PAGE_TABLE_SIZE);
	}
}
