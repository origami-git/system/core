/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/paging/frame_manager.hpp>
#include <util/std/mem.hpp>

namespace red_moon::mem::paging {

	red_moon::util::bitmap PageFrameManager::frame_map;

	page_frame* PageFrameManager::alloc() {
		assert((m_free - PAGE_SIZE) > 0, "out of memory")
		static page_frame frame;

		for(uint64 i = recent_index; i < frame_map.buffer_size; i++) {
			if(!frame_map.get(i)) {
				frame_map.set(i, true);
				recent_index = i;

				frame.index = i;
				frame.paddress = (void*)(memory_base + (i * PAGE_SIZE));

				m_used += PAGE_SIZE;
				m_free -= PAGE_SIZE;

				memset(frame.paddress, 0, PAGE_SIZE);
				return &frame;
			}
		}

		#pragma GCC diagnostic ignored "-Wtype-limits"
		for(uint64 i = recent_index - 1; i >= 0; i--) {
			if(!frame_map.get(i)) {
				frame_map.set(i, true);
				recent_index = i;

				frame.index = i;
				frame.paddress = (void*)(memory_base + (i * PAGE_SIZE));

				m_used += PAGE_SIZE;
				m_free -= PAGE_SIZE;

				memset(frame.paddress, 0, PAGE_SIZE);
				return &frame;
			}

			if(i == 0) break;
		}

		return nullptr;
	}

	size_t PageFrameManager::alloc(size_t count, page_frame** __out frames /*= nullptr*/) {
		size_t correct = 0;

		for(size_t i = 0; i < count; i++) {
			page_frame* frame = alloc();

			if(frames != nullptr) *frames[i] = frame;
			if(frame != nullptr) correct++;
		}

		return correct;
	}

	page_frame* PageFrameManager::lock(void* paddress) {
		size_t index = ((size_t) paddress - memory_base) / PAGE_SIZE;
		recent_index = index;

		if(frame_map.set(index, true)) return nullptr;

		m_used += PAGE_SIZE;
		m_free -= PAGE_SIZE;

		static page_frame frame = page_frame {
			.index = index,
			.paddress = paddress
		};
		return &frame;
	}

	size_t PageFrameManager::lock(void* paddresses[], size_t count, page_frame** __out frames /*= nullptr*/) {
		size_t correct = 0;

		for(size_t i = 0; i < count; i++) {
			page_frame* frame = lock(paddresses[i]);

			if(frames != nullptr) *frames[i] = frame;
			if(frame != nullptr) correct++;
		}

		return correct;
	}

	size_t PageFrameManager::lock(void* paddress, size_t count, page_frame** __out frames /*= nullptr*/) {
		size_t correct = 0;

		for(size_t i = 0; i < count; i++) {
			page_frame* frame = lock((void*) ((size_t) paddress + (i * PAGE_SIZE)));

			if(frames != nullptr) *frames[i] = frame;
			if(frame != nullptr) correct++;
		}

		return correct;
	}

	bool PageFrameManager::free(void* paddress) {
		uint64 index = ((uint64) paddress - memory_base) / PAGE_SIZE;
		recent_index = index;

		m_used -= PAGE_SIZE;
		m_free += PAGE_SIZE;

		return frame_map.set(index, false) != false;
	}

	size_t PageFrameManager::free(void* paddresses[], size_t count) {
		size_t correct = 0;

		for(size_t i = 0; i < count; i++) {
			if(free(paddresses[i])) correct++;
		}

		return correct;
	}

	size_t PageFrameManager::free(void* paddress, size_t count) {
		size_t correct = 0;

		for(size_t i = 0; i < count; i++) {
			if(free((void*) ((size_t) paddress + (i * PAGE_SIZE)))) {
				correct++;
			}
		}

		return correct;
	}
}
