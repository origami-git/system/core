/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/paging/directory_manager.hpp>
#include <util/std/math.hpp>
#include <util/types.hpp>

namespace red_moon::mem::paging {

	PageDirectoryEntry PageDirectoryManager::entries[PAGE_DIRECTORY_SIZE] __attribute__((aligned(4096))); 
	PageTableManager* PageDirectoryManager::tables[PAGE_TABLE_SIZE] __attribute__((aligned(0x1000)));

	void* PageDirectoryManager::get() {
		return &entries;
	}

	PageDirectoryEntry* PageDirectoryManager::entry(uint16 index) {
		assert(index < PAGE_DIRECTORY_SIZE, "index > PAGE_DIRECTORY_SIZE")
		return &entries[index];
	}

	PageDirectoryEntry* PageDirectoryManager::entry(void* address) {
		return entry(((size_t) address) / 0x1000 / PAGE_DIRECTORY_SIZE);
	}

	PageTableManager* PageDirectoryManager::table(uint16 index) {
		assert(index < PAGE_TABLE_SIZE, "index > PAGE_TABLE_SIZE")
		PageTableManager* ptm = tables[index];

		if(ptm == nullptr) {
			ptm = new ((void*) frame_manager->alloc()->paddress) PageTableManager();
			tables[index] = ptm;

			PageDirectoryEntry* pde = entry(index);
			pde->address(true, (uint32) ptm->get());
		}

		return ptm;
	}

	PageTableManager* PageDirectoryManager::table(void* address) {
		return table(((size_t) address) / 0x1000 / PAGE_TABLE_SIZE);
	}
}
 