/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/heap/allocator.hpp>
#include <mem/paging.hpp>

namespace red_moon::mem::heap {

	void* Allocator::alloc(size_t size) {
		assert(size > 0, "size < 0")

		if(size % PAGE_SIZE > 0) {
			size -= (size % PAGE_SIZE);
			size += PAGE_SIZE;
		}

		fragment* current = heap->recent_header->prev == nullptr ?
			(fragment*) heap->start_address : heap->recent_header->prev;

		do {
			if(current->free) {
				if(current->size > size) current->split(size);
				else if(current->size < size) continue;

				current->free = false;
				return (void*) ((size_t) current + sizeof(fragment));
			}
		} while((current = current->next) != nullptr);

		/* heap not big enough */
		heap->expand(size);
		return alloc(size);
	}

	void Allocator::free(void* address) {
		fragment* heap_fragment = (fragment*) address - 1;
		heap_fragment->free = true;
		heap_fragment->join(true);
		heap_fragment->join(false);
	}
}
