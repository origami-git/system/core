/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/heap/fragment.hpp>
#include <mem/heap/manager.hpp>

namespace red_moon::mem::heap {

	fragment* fragment::split(size_t size) {
		if(size < PAGE_SIZE) return nullptr;

		size_t split_fragment_size = this->size - size - sizeof(fragment);
		if(split_fragment_size < PAGE_SIZE) return nullptr;

		fragment* split_fragment = (fragment*) ((size_t) this + size + sizeof(fragment));
		split_fragment->heap = heap;
		next->prev = split_fragment;
		split_fragment->next = next;
		next = split_fragment;
		split_fragment->prev = this;
		split_fragment->size = split_fragment_size;
		split_fragment->free = free;
		this->size = size;

		if(((Heap*) heap)->recent_header == this) ((Heap*) heap)->recent_header = split_fragment;
		return split_fragment;
	}

	void fragment::join(bool forward) {
		if(forward) {
			if(!next) return;
			if(!next->free) return;

			if(next == ((Heap*) heap)->recent_header) ((Heap*) heap)->recent_header = this;
			if(next->next) next->next->prev = this;

			size = size + next->size + sizeof(fragment);
		} else {
			if(prev && prev->free) prev->join(true);
		}
	}
}
