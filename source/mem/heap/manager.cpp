/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/heap/manager.hpp>
#include <mem/heap/fragment.hpp>

namespace red_moon::mem::heap {

	void Heap::expand(size_t size) {
		if(size % PAGE_SIZE) {
			size -= size % PAGE_SIZE;
			size += PAGE_SIZE;
		}

		size_t page_count = size / PAGE_SIZE;
		heap::fragment* new_fragment = (heap::fragment*) end_address;

		for(size_t i = 0; i < page_count; i++) {
			map_memory(paging::frame_manager.alloc()->paddress, (void*) ((size_t) end_address + i));
			end_address += PAGE_SIZE;
		}

		new_fragment->heap = this;

		new_fragment->free = true;
		new_fragment->prev = recent_header;

		recent_header->next = new_fragment;
		recent_header = new_fragment;

		new_fragment->next = NULL;
		new_fragment->size = size - sizeof(heap::fragment);
		new_fragment->join(false);
	}
}
