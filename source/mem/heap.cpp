/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <mem/heap.hpp>
#include <mem/paging.hpp>

using namespace red_moon::mem::heap;

namespace red_moon::mem {

	namespace heap {

		Heap manager;
		Allocator allocator;
	}

	bool heap_init(void* address, size_t size) {
		manager = Heap(address, size);
		allocator = Allocator(&manager);

		return true;
	}
}
