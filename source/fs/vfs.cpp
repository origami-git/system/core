/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <fs/vfs.hpp>

namespace red_moon::fs {

	util::Dictionary<const char*, mount_point*>* mounts;

	bool vfs_init() {
		mounts = new util::Dictionary<const char*, mount_point*>;

		/* load drivers here possibly,
		mount sysfs/devfs (unless that'll be done by init),
		etc */
	}
}
