/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <fs/io.hpp>
#include <fs/vfs.hpp>
#include <util/std/uno.hpp>
#include <mod/fs.hpp>

namespace red_moon::fs {

	inline bool __getreq(const char* path, mount_point* __ref mp, char* real[]) {
		#define ITEM util::Dictionary<const char*, mount_point*>::Item

		if(!mounts->first) return false;
		ITEM* current = mounts->first;

		size_t count = 0;
		strsplit(path, ':', real, 2, count);

		do {
			if(strcmp(real[0], current->key) == 0) {
				mp = current->value;
				return true;
			}
		} while((current = current->next) != nullptr);

		return false;
	}

	#define __precall(path) mount_point* mp; char* real[2] = { 0 }; \
							if(!__getreq(path, mp, real)) return false;

	bool fcreate(const char* path, f_context __ref ctx) {
		__precall(path)
		return (dynamic_cast<mod::FilesystemDriver*>(mp->driver))->_create(*mp, real[1], 0, ctx);
	}

	bool fdelete(const char* path) {
		__precall(path)
		return (dynamic_cast<mod::FilesystemDriver*>(mp->driver))->_delete(*mp, real[1]);
	}
	
	bool mkdir(const char* path) {
		__precall(path)

		f_context ctx;
		return (dynamic_cast<mod::FilesystemDriver*>(mp->driver))->_create(*mp, real[1], 1, ctx);
	}

	bool rmdir(const char* path) {
		__precall(path)
		// return (dynamic_cast<mod::FilesystemDriver*>(mp->driver))->_rmdir(*mp, real[1]);
		return false; /* not implemented */
	}

	size_t ls(const char* path, inode* __out files) {
		__precall(path);
		return (dynamic_cast<mod::FilesystemDriver*>(mp->driver))->_list(*mp, real[1], files);
	}

	bool fopen(const char* path, f_context __ref ctx) {
		__precall(path)
		return (dynamic_cast<mod::FilesystemDriver*>(mp->driver))->_open(*mp, real[1], ctx);
	}

	bool fclose(f_context __ref ctx) {
		return (dynamic_cast<mod::FilesystemDriver*>(ctx._mount.driver))
			->_close(ctx._mount, ctx);
	}

	bool rename(f_context ctx, const char* name) {
		return (dynamic_cast<mod::FilesystemDriver*>(ctx._mount.driver))
			->_rename(ctx._mount, ctx, name);
	}

	size_t fread(f_context __ref ctx, size_t begin, size_t end, byte __out data) {
		return (dynamic_cast<mod::FilesystemDriver*>(ctx._mount.driver))
			->_read(ctx._mount, ctx, begin, end, data);
	}

	size_t fwrite(f_context __ref ctx, byte __in data, size_t begin, size_t length) {
		return (dynamic_cast<mod::FilesystemDriver*>(ctx._mount.driver))
			->_write(ctx._mount, ctx, data, begin, length);
	}
}
