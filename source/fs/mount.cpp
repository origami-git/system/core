/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <fs/mount.hpp>
#include <mod/driver.hpp>
#include <util/std/string.hpp>
#include <panic.hpp>
#include <fs/vfs.hpp>
#include <util/std/uno.hpp>

namespace red_moon::fs {

	bool mount(const char* device, const char* path, const char* driver) {
		static mount_point mp;
		mp.path = path;

		/* we don't have an actual device manager yet,
		so we treat everything as 'nodev' */
		hw::Device* dev = nullptr;
		mp.device = dev;

		mod::Driver* drv = mod::driver::get("fs", driver);

		if(drv == nullptr) {
			return false;
		}

		mod::FilesystemDriver* fsdrv = dynamic_cast<mod::FilesystemDriver*>(drv);
		mp.driver = fsdrv;

		char* real[2] = { 0 };
		size_t count = 0;

		strsplit(path, ':', real, 2, count);

		if(real[1] == nullptr || strcmp(real[1], "/") == 0 || strcmp(real[1], "\0") == 0) {
			if(mounts->has(real[0])) return false;

			mp.path = real[0];

			if(!fsdrv->_init_mount(mp)) return false;
			mounts->insert(mp.path, &mp);
		} else {
			if(!mounts->has(real[0])) return false;

			mp.path = real[0];
			if(!fsdrv->_init_mount(mp, real[1])) return false;
		}

		return true;
	}

	bool unmount(const char* path) {
		mount_point* mp;

		char* real[2] = { 0 };
		size_t count = 0;

		strsplit(path, ':', real, 2, count);
		if(!mounts->get(real[0], mp)) return false;

		mod::FilesystemDriver* fsdrv = (dynamic_cast<mod::FilesystemDriver*>(mp->driver));
		mp->path = real[0];

		if(real[1] == nullptr || strcmp(real[1], "\0") == 0 || strcmp(real[1], "/") == 0) {
			if(!fsdrv->_deinit_mount(*mp)) return false;
			if(!mounts->remove(real[0])) return false;
		} else {
			if(!fsdrv->_deinit_mount(*mp, real[1])) return false;
		}

		return true;
	}
}
