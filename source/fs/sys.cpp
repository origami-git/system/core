/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#ifdef MOD_DRV_FS_SYSFS
#include <fs/sys.hpp>
#include <util/std/uno.hpp>

namespace red_moon::fs {

	inline bool __getreq(const char* path, mount_point* __ref mp, char* real[]) {
		#define ITEM util::Dictionary<const char*, mount_point*>::Item

		if(!mounts->first) return false;
		ITEM* current = mounts->first;

		size_t count = 0;
		strsplit(path, ':', real, 2, count);

		do {
			if(strcmp(real[0], current->key) == 0) {
				mp = current->value;
				return true;
			}
		} while((current = current->next) != nullptr);

		return false;
	}

	#define __precall(path) mount_point* mp; char* real[2] = { 0 }; \
							if(!__getreq(path, mp, real)) return false;

	bool sysio_register(const char* path, bool (*handler)(bool, byte*, size_t __ref)) {
		mod::Driver* driver = mod::driver::get("fs", "sysfs");
		mod::drivers::SysfsDriver* sysfs = dynamic_cast<mod::drivers::SysfsDriver*>(driver);

		__precall(path);
		sysfs->sysio_export(*mp, real[1], handler);
	}

	bool sysio_remove(const char* path) {
		mod::Driver* driver = mod::driver::get("fs", "sysfs");
		mod::drivers::SysfsDriver* sysfs = dynamic_cast<mod::drivers::SysfsDriver*>(driver);

		__precall(path);
		sysfs->sysio_unreg(*mp, real[1]);
	}
}
#endif
