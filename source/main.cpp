/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <multiboot2.h>
#include <io/early_serial.hpp>
#include <mem/paging.hpp>
#include <util/types.hpp>
#include <util/std/string.hpp>
#include <mem/heap.hpp>
#include <util/std/mem.hpp>
#include <mod/driver.hpp>
#include <mod/drivers/example.hpp>
#include <mod/drivers/fs/ramfs.hpp>
#include <fs/mount.hpp>
#include <fs/io.hpp>
#include <fs/vfs.hpp>

using namespace red_moon;

extern "C" void kmain(unsigned long magic, unsigned long address) {
	if(magic != MULTIBOOT2_BOOTLOADER_MAGIC) {
		return;
	}

	if(!eserial_init(COM1)) {
		return;
	}

	struct multiboot_tag *tag;
	// unsigned long size = *(unsigned*) address;

	uint32 mem_lower;
	uint32 mem_upper;

	uint64 mem_size;
	uint64 mem_start;

	eserial_write("Hello, world!\n\n");

	for(tag = (struct multiboot_tag *) (address + 8);
		tag->type != MULTIBOOT_TAG_TYPE_END;
		tag = (struct multiboot_tag *) ((multiboot_uint8_t *) tag + ((tag->size + 7) & ~7))) {
		
		switch(tag->type) {
			case MULTIBOOT_TAG_TYPE_BASIC_MEMINFO: {
				mem_lower = ((struct multiboot_tag_basic_meminfo*) tag)->mem_lower;
				mem_upper = ((struct multiboot_tag_basic_meminfo*) tag)->mem_upper;
				break;
			}
			case MULTIBOOT_TAG_TYPE_MMAP: {
				uint32 map_size = ((struct multiboot_tag_mmap*) tag)->entry_size;
				multiboot_mmap_entry* map = ((struct multiboot_tag_mmap*) tag)->entries;

				for(; (byte*) map < (byte*) tag + tag->size;
					map = (multiboot_mmap_entry*) ((uint32) map
                    	+ ((struct multiboot_tag_mmap*) tag)->entry_size)) {

					if(map->len > mem_size) {
						mem_size = map->len;
						mem_start = map->addr;
					}
				}

				break;
			}
		}
	}

	*(reinterpret_cast<uint16_t*>(0xB8000)) = static_cast<uint16_t>('N' | 0x0F00);

	mem::paging_init((void*)(size_t) mem_start, mem_size);
	eserial_write("Initialized paging\n");

	/* TODO: try to get the last currently allocated bitmap address
			 and allocate to that + 1 */
	for(size_t i = mem_start;
		i <= 0x200000;
		i += 4096) {

		mem::map_memory((void*) i, (void*) i);
	}

	mem::paging_enable();
	eserial_write("Enabled paging\n");

	eserial_write("Initializing heap");
	mem::heap_init(mem::paging::frame_manager.alloc()->paddress, 0x1000);
	eserial_write(" (OK)\n\n");

	uint64* array = new uint64[8];
	
	for(uint64 i = 0; i < 8; i++) {
		array[i] = i ^ 2;
	}

	// uint32* array0 = new uint32[0x2000 + 1];

	eserial_write("Mapping physical address 0xB8000 to 0xCF000000");
	mem::map_memory((void*) 0xB8000, (void*) 0xCF000000);
	eserial_write(" (OK)\n\n");

	*(reinterpret_cast<uint16_t*>(0xCF000000 + 2)) = static_cast<uint16_t>('P' | 0x0F00);

	/* driver example test */
#ifdef MOD_DRV_EXAMPLE
	eserial_write("Loading example driver\n");
	mod::driver::load(new mod::drivers::ExampleDriver);

	eserial_write("Enabling example driver: ");
	mod::Driver* driver = mod::driver::get("none", "example");
	mod::drivers::ExampleDriver* example = dynamic_cast<mod::drivers::ExampleDriver*>(driver);

	char buf[8];

	itoa(example->_entrypoint(), buf, 10);
	eserial_write(buf);

	eserial_write("\nDisabling example driver: ");
	itoa(example->_exit(), buf, 10);
	eserial_write(buf);

	eserial_write("\nUnloading example driver");
	mod::driver::unload(driver);

	eserial_write("\n\n");

	//delete driver;
#endif

	fs::vfs_init();

#ifdef MOD_DRV_FS_RAMFS
	eserial_write("Loading ramfs driver\n");
	mod::driver::load(new mod::drivers::RamfsDriver);

	mod::Driver* driver1 = mod::driver::get("fs", "ramfs");
	mod::drivers::RamfsDriver* ramfs = dynamic_cast<mod::drivers::RamfsDriver*>(driver1);

	eserial_write("Mounting ramfs at root:/");
	if(fs::mount("nodev", "root:", "ramfs")) {
		eserial_write(" (OK)\n");
	} else {
		eserial_write(" (FAILED)\n");
		return;
	}

	eserial_write("Creating file root:/hello.txt");
	f_context handle;

	if(fs::fcreate("root:/hello.txt", handle)) {
		eserial_write(" (OK)\n");
	} else {
		eserial_write(" (FAILED)\n");
		return;
	}

	eserial_write("Writing 'world' to root:/hello.txt");
	const char* hello = "world";

	if(fs::fwrite(handle, (byte*) hello, 0, strlen(hello) + 1 /* +1 for \0 */)) {
		eserial_write(" (OK)\n");
	} else {
		eserial_write(" (FAILED)\n");
		return;
	}

	eserial_write("Reading file root:/hello.txt: ");

	byte read[8] = { 0 };
	size_t count = fs::fread(handle, 0, handle._data_size, read);

	for(size_t i = 0; i < count; i++) {
		eserial_write(read[i]);
	}

	eserial_write("\nClosing context to file");
	fs::fclose(handle);

	eserial_write("\n\n");
#endif

	/* eserial_write("Unmapping virtual address 0xCF000000");
	mem::unmap_memory((void*) 0xCF000000);
	eserial_write(" (OK)\n");

	*(reinterpret_cast<uint16_t*>(0xCF000000 + 4)) = static_cast<uint16_t>('U' | 0x0F00); */
}
