;	This Source Code Form is subject to the terms of the Mozilla Public
;	License, v. 2.0. If a copy of the MPL was not distributed with this
;	file, You can obtain one at http://mozilla.org/MPL/2.0/.

section .text
global _page_directory_load:function
global _paging_enable:function

_page_directory_load:
	push ebp
	mov ebp, esp
	mov eax, [ebp+8]
	mov cr3, eax
	mov esp, ebp
	pop ebp
	ret

_paging_enable:
	mov eax, cr0
	or eax, 0x80000000
	mov cr0, eax
	ret
