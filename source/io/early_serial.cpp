/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <io/early_serial.hpp>
#include <util/std/string.hpp>

unsigned short port = 0;

bool eserial_init(unsigned short _port) {
	outb(_port + 1, 0x00);		// Disable all interrupts
	outb(_port + 3, 0x80);		// Enable DLAB (set baud rate divisor)
	outb(_port + 0, BAUD_RATE);	// Set divisor to 3 (lo byte) 38400 baud
	outb(_port + 1, 0x00);		//                  (hi byte)
	outb(_port + 3, 0x03);		// 8 bits, no parity, one stop bit
	outb(_port + 2, 0xC7);		// Enable FIFO, clear them, with 14-byte threshold
	outb(_port + 4, 0x0B);		// IRQs enabled, RTS/DSR set
	outb(_port + 4, 0x1E);		// Set in loopback mode, test the serial chip
	outb(_port + 0, 0xAE);		// Test serial chip (send byte 0xAE and check if serial returns same byte)
 
	// Check if serial is faulty (i.e: not same byte as sent)
	if(inb(_port + 0) != 0xAE) {
		return false;
	}
 
	// If serial is not faulty set it in normal operation mode
	// (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
	outb(_port + 4, 0x0F);

	port = _port;
	return true;
}

char eserial_read() {
	while(!_eserial_available_read());
	return inb(port);
}

void eserial_readline(char** buffer) {
	char read = '\0';
	size_t i = 0;

	while((read = eserial_read()) != '\n') {
		(*buffer)[i] = read;
		i++;
	}
}

void eserial_write(char a) {
	while(!_eserial_able_send());
	outb(port, a);
}

void eserial_write(const char* s) {
	for(size_t i = 0; i < strlen(s); i++) {
		eserial_write(s[i]);
	}
}
